import {NgModule} from '@angular/core';
import {
    MdButtonModule, MdToolbarModule, MdListModule, MdInputModule,
    MdSelectModule, MdCardModule, MdSnackBarModule, MdIconModule, MdProgressSpinnerModule
} from "@angular/material";

@NgModule({
    imports: [MdButtonModule, MdToolbarModule, MdListModule, MdInputModule, MdSelectModule, MdCardModule, MdSnackBarModule, MdIconModule, MdProgressSpinnerModule],
    exports: [MdButtonModule, MdToolbarModule, MdListModule, MdInputModule, MdSelectModule, MdCardModule, MdSnackBarModule, MdIconModule, MdProgressSpinnerModule],
    declarations: [],
    providers: [],
})
export class SharedModule {
}
