import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'top-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})

export class MenuComponent {
    items = [{
        path: '/albums',
        name: 'albums'
    }, {
        path: '/authors',
        name: 'authors'
    }];
}