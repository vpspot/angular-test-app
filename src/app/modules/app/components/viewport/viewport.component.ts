import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'app-root',
    templateUrl: './viewport.component.html'
})
export class ViewportComponent {}
