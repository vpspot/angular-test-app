import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import {ViewportComponent} from "./components/viewport/viewport.component";
import {MenuComponent} from "./components/menu/menu.component";
import {AlbumService} from "../../services/album.service";
import {AuthorService} from "../../services/author.service";
import {AppRoutingModule} from "./app-routing.module";
import {AlbumModule} from "../album/album.module";
import {AuthorModule} from "../author/author.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../shared/shared.module";
import {AuthorResolve} from "../../services/author.resolve";
import {AlbumResolve} from "../../services/album.resolve";
import {AlbumListResolve} from "../../services/album-list.resolve";
import {AuthorListResolve} from "../../services/author-list.resolve";

@NgModule({
    declarations: [
        ViewportComponent,
        MenuComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        AlbumModule,
        AuthorModule,
        BrowserAnimationsModule,
        SharedModule
    ],
    providers: [AuthorService, AlbumService, AuthorResolve, AuthorListResolve, AlbumResolve, AlbumListResolve],
    bootstrap: [ViewportComponent]
})
export class AppModule { }
