import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AuthorListComponent} from "./components/author-list/author-list.component";
import {AuthorDetailComponent} from "./components/author-detail/author-detail.component";
import {AuthorResolve} from "../../services/author.resolve";
import {AuthorListResolve} from "../../services/author-list.resolve";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'authors', component: AuthorListComponent, resolve: {
            authors: AuthorListResolve
        }},
        { path: 'authors/:id', component: AuthorDetailComponent, resolve: {
            author : AuthorResolve
        }}
    ])],
    exports: [RouterModule]
})
export class AuthorRoutingModule {}