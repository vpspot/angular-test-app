import {NgModule} from '@angular/core';
import {AuthorListComponent} from "./components/author-list/author-list.component";
import {AuthorDetailComponent} from "./components/author-detail/author-detail.component";
import {AuthorRoutingModule} from "./author-router.module";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";

@NgModule({
    imports: [CommonModule, FormsModule, AuthorRoutingModule, SharedModule],
    exports: [AuthorListComponent, AuthorDetailComponent],
    declarations: [AuthorListComponent, AuthorDetailComponent],
    providers: [],
})
export class AuthorModule {

}
