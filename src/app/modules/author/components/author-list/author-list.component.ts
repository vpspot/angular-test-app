import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Author} from "../../../../models/author.model";

@Component({
    moduleId: module.id,
    selector: 'authors',
    templateUrl: './author-list.component.html',
    styleUrls: ['author-list.component.css']
})

export class AuthorListComponent implements OnInit {
    title = 'Authors';
    authors: Author[];

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.authors = this.route.snapshot.data['authors'];
    }

    onRecordClick(author: Author): void {
        this.router.navigate(['/authors', author.id]);
    }
}