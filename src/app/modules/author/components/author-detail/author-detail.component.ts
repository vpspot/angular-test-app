import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthorService} from "../../../../services/author.service";
import {Author} from "../../../../models/author.model";
import {MdSnackBar} from "@angular/material";

@Component({
    moduleId: module.id,
    selector: 'author-detail',
    templateUrl: 'author-detail.component.html'
})

export class AuthorDetailComponent implements OnInit {
    title = 'Author details';
    author: Author;
    submitted = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authorService: AuthorService,
        private snackBar: MdSnackBar
    ) {}

    ngOnInit(): void {
        this.author = this.route.snapshot.data['author'];
    }

    saveRecord(): void {
        this.submitted = true;
        this.authorService.update(this.author).subscribe((author) => {
            this.submitted = false;
            this.snackBar.open(`Album #${author.id} has been saved`, 'OK', {
                duration: 2000
            });
        });
    }

    goBack(): void {
        this.router.navigate(['/authors']);
    }

    onSubmit() {
        this.saveRecord();
    }
}