import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AlbumListComponent} from "./components/album-list/album-list.component";
import {AlbumDetailComponent} from "./components/album-detail/album-detail.component";
import {AlbumResolve} from "../../services/album.resolve";
import {AlbumListResolve} from "../../services/album-list.resolve";

@NgModule({
    imports: [RouterModule.forChild([
        { path: 'albums', component: AlbumListComponent, resolve: {
            albums: AlbumListResolve
        } },
        { path: 'albums/:id', component: AlbumDetailComponent, resolve: {
            album: AlbumResolve
        } }
    ])],
    exports: [RouterModule]
})
export class AlbumRoutingModule {}