import {NgModule} from '@angular/core';
import {AlbumListComponent} from "./components/album-list/album-list.component";
import {AlbumDetailComponent} from "./components/album-detail/album-detail.component";
import {AlbumRoutingModule} from "./album-routing.module";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";


@NgModule({
    imports: [CommonModule, FormsModule, AlbumRoutingModule, SharedModule],
    exports: [AlbumListComponent, AlbumDetailComponent],
    declarations: [AlbumListComponent, AlbumDetailComponent],
    providers: [],
})
export class AlbumModule {
}
