import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Album} from "../../../../models/albums.model";

@Component({
    moduleId: module.id,
    selector: 'album-list',
    templateUrl: './album-list.component.html',
    styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
    title = 'albums';
    albums: Album[];

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.albums = this.route.snapshot.data['albums'];
    }

    onRecordClick(album: Album): void {
        this.router.navigate(['/albums', album.id]);
    }
}