import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router}   from '@angular/router';

import 'rxjs/add/operator/switchMap';
import {Album} from "../../../../models/albums.model";
import {AlbumService} from "../../../../services/album.service";
import {Author} from "../../../../models/author.model";
import {AuthorService} from "../../../../services/author.service";
import {MdSnackBar} from "@angular/material";

@Component({
    moduleId: module.id,
    selector: 'album-detail',
    templateUrl: './album-detail.component.html'
})
export class AlbumDetailComponent implements OnInit {
    album   : Album;
    authors : Author[];
    submitted = false;
    title = 'Album details';

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private albumService: AlbumService,
        private authorService: AuthorService,
        private snackBar: MdSnackBar
    ) {}

    ngOnInit(): void {
        this.album = this.route.snapshot.data['album'];

        this.authorService.getAuthors().subscribe(authors => this.authors = authors);
    }

    saveRecord() {
        this.submitted = true;
        return this.albumService.update(this.album).subscribe((album)=>{
            this.submitted = false;
            this.snackBar.open(`Album #${album.id} has been saved`, 'OK', {
                duration: 2000
            });
        });
    }

    goBack(): void {
        this.router.navigate(['/albums']);
    }

    onSubmit() {
        this.saveRecord();
    }
}