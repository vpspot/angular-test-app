import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {Album} from "../models/albums.model";
import {AlbumService} from "./album.service";

@Injectable()
export class AlbumListResolve implements Resolve<Album[]> {

    constructor(private albumService: AlbumService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.albumService.getAlbums().map((albums) => {
            return albums.sort((a: Album, b: Album) => {
                if (a.title == b.title) return 0;
                else return a.title > b.title ? 1 : -1;
            });
        });
    }
}