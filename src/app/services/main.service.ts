import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";

@Injectable()
export class MainService {

    constructor() {
    }

    protected handleError(error: any): Observable<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Observable.of<any[]>([]);
    }

}