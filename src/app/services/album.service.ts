import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";

import {Album} from "../models/albums.model";
import {MainService} from "./main.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AlbumService extends MainService {
    private serviceUrl = 'http://jsonplaceholder.typicode.com/albums';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
        super();
    }

    getAlbums(): Observable<Album[]> {
        return this.http.get(this.serviceUrl)
            .map(response => response.json() as Album[])
            .catch(this.handleError);
    }

    getAlbum(id: number): Observable<Album> {
        const url = `${this.serviceUrl}/${id}`;
        return this.http.get(url)
            .map(response => response.json() as Album)
            .catch(this.handleError);
    }

    update(album: Album): Observable<Album> {
        const url = `${this.serviceUrl}/${album.id}`;
        return this.http
            .put(url, JSON.stringify(album), {headers: this.headers})
            .map(response => response.json() as Album)
            .catch(this.handleError);
    }
}