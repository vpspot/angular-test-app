import {Injectable} from '@angular/core';
import {Http, Headers} from "@angular/http";
import {Author} from "../models/author.model";
import {MainService} from "./main.service";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AuthorService extends MainService {
    private serviceUrl = 'http://jsonplaceholder.typicode.com/users';
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http) {
        super();
    }

    getAuthors(): Observable<Author[]> {
        return this.http.get(this.serviceUrl)
            .map(response => response.json() as Author[])
            .catch(this.handleError);
    }

    getAuthor(id: number): Observable<Author> {
        const url = `${this.serviceUrl}/${id}`;
        return this.http.get(url)
            .map(response => response.json() as Author)
            .catch(this.handleError);
    }

    update(author:Author): Observable<Author> {
        const url = `${this.serviceUrl}/${author.id}`;
        return this.http
            .put(url, JSON.stringify(author), {
                headers: this.headers
            })
            .map(response => response.json() as Author)
            .catch(this.handleError);
    }
}