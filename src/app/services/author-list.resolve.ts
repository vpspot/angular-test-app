import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {Author} from "../models/author.model";
import {AuthorService} from "./author.service";

@Injectable()
export class AuthorListResolve implements Resolve<Author[]> {

    constructor(private authorService: AuthorService) {}

    resolve(route: ActivatedRouteSnapshot) {
        return this.authorService.getAuthors();
    }
}