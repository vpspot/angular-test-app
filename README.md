# Angular app
* Use Angular2 framework and angular-material library for UI.
* Set up the application to use router to provider 2+ routes (views/pages).
* Have each route use a dedicated component to bind data to the view file.
* Call a service to retrieve and pass back data from a REST endpoint. (use this sample endpoint: http://jsonplaceholder.typicode.com/posts/1)
* Display data from the endpoint on the view
    * Create a list view with links to a view that shows individual records
	* The idividual record view should allow for the ability to edit that
* information in a form and send it back to an endpoint with some basic client validation.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
